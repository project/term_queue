<?php
/**
 * @file
 * Administrative page callbacks for the term_queue module.
 */

/**
 * Render the listing of all available term queues.
 */
function term_queue_admin_list() {
  $queues = term_queue_list();  
  
  $rows = array();
  foreach ( $queues as $obj ) {
    $row = array();
    $row[] = check_plain($obj->title);
    $row[] = check_plain($obj->description);
    $row[] = l(t('Edit Queue'), 'admin/build/term_queue/'. $obj->qid . '/edit'); 
    $row[] = l(t('Edit Terms'), 'admin/build/term_queue/'. $obj->qid); 
    $row[] = l(t('Delete'), 'admin/build/term_queue/'. $obj->qid . '/delete');         

    $row = array('data' => $row);
    $rows[] = $row;
  }
  
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No queues have been created'), 'colspan' => '3'));
  }
  
  $header = array(t('Title'), t('Description'), array('data' => t('Operations'), 'colspan' => '3'));
  $output = theme('table', $header, $rows, array('id' => 'term-queue'));
  return $output;
}

/**
 * Form for editing the details of a queue
 *
 * @ingroup forms
 * @see term_queue_form_submit()
 */
function term_queue_form(&$form_state, $edit = array()) {
  if (isset($edit['qid'])) {
    $form['qid'] = array('#type' => 'value', '#value' => $edit['qid']);
  }
  $form['title'] = array('#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $edit['title'],
    '#maxlength' => 255,
    '#description' => t('The name for this term queue, e.g., <em>"Top 10"</em>.'),
    '#required' => TRUE,
  );
  $form['description'] = array('#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $edit['description'],
    '#description' => t('Description of the term queue; can be used by modules.'),
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  $form['cancel'] = array(
    '#type' => 'markup', 
    '#value' => l(t('Cancel'), 'admin/build/term_queue'), 
    '#weight' => 21
  );

  return $form;
}

/**
 * Submit handler for term queue.
 *
 * @see term_queue_form()
 */
function term_queue_form_submit($form, &$form_state) {
  term_queue_save($form_state['values']);
  $form_state['redirect'] = 'admin/build/term_queue/list';
  return;
}

/**
 * Form to edit an existing term queue
 *
 * @ingroup forms
 */
function term_queue_edit_form($edit = array()) {
  return drupal_get_form('term_queue_form', (array)$edit);
}

/**
 * Form builder for the queue delete form.
 *
 * @ingroup forms
 * @see term_queue_delete_form_submit()
 */
function term_queue_delete_form(&$form_state, $queue) {
  $form['qid'] = array('#type' => 'value', '#value' => $queue->qid);

  return confirm_form($form,
    t('Are you sure you want to delete the queue %field?', array('%field' => $queue->title)), 
    'admin/build/term_queue/list',
    NULL,
    t('Delete'), 
    t('Cancel'));
}

/**
 * Submit handler to delete a queue after confirmation, pull the trigger.
 *
 * @see term_queue_delete_form()
 */
function term_queue_delete_form_submit($form, &$form_state) {
  term_queue_delete($form_state['values']);
  $form_state['redirect'] = 'admin/build/term_queue/list';
  return;
}

/**
 * Form function for adding terms to a queue.
 *
 * @ingroup forms
 * @see term_queue_add_term_form_submit()
 */
function term_queue_add_term_form(&$form_state, $queue) {

  drupal_set_title(t('%queue', array('%queue' => $queue->title)));

  $vocab_terms = array();
  $vocabs = taxonomy_get_vocabularies();
  foreach ($vocabs as $vid => $vocab) {
    $vocab_terms[$vid] = taxonomy_get_tree($vid);
  }
  
  $options = array();
  foreach ($vocab_terms as $vid => $terms) {
    $vocab_name = $vocabs[$vid]->name;
    $option[$vocab_name] = array();
    foreach ($terms as $term) {
      $choice = new stdClass();
      $choice->option = array($term->tid => str_repeat('-', $term->depth) . $term->name);
      $options[$vocab_name][] = $choice;
    }
  }

  $form['qid'] = array(
    '#type' => 'value',
    '#value' => $queue->qid,
  );

  $form['tid'] = array(
    '#type' => 'select',
    '#title' => t('Term'),
    '#options' => $options,
    '#description' => t('Select a term to be added to the queue.'),
    '#weight' => -15,
    '#theme' => 'taxonomy_term_select',
  );
  
  $form['submit'] = array('#type' => 'submit', '#value' => 'Add Term');
  $form['cancel'] = array(
    '#type' => 'markup', 
    '#value' => l(t('Cancel'), 'admin/build/term_queue/' . $queue->qid), 
    '#weight' => 21
  );

  return $form;
}

/**
 * Submit handler to add a term to queue.
 *
 * @see term_queue_add_term_form()
 */
function term_queue_add_term_form_submit($form, &$form_state) {
  $qid = $form_state['values']['qid'];
  $tid = $form_state['values']['tid'];
  term_queue_add_term($qid, $tid);
  $form_state['redirect'] = "admin/build/term_queue/$qid/add";
  
  $term_queue = term_queue_load($qid);
  $term = taxonomy_get_term($tid);
  
  
  drupal_set_message(t('%term has been saved to %queue.', array('%term' => $term->name, '%queue' => $term_queue->title)));
  return;
}

/**
 * Form builder for the queue terms overview.
 *
 * Display all the terms in a queue, with option to delete
 * each one. The form is made drag and drop by the theme function.
 *
 * @ingroup forms
 * @see term_queue_term_form_submit()
 * @see theme_term_queue_term_form()
 */
function term_queue_term_form(&$form_state, $queue) {
  
  drupal_set_title(t('%queue', array('%queue' => $queue->title)));

  $form = array(
    '#queue' => (array)$queue,
    '#tree' => TRUE,
  );

  $qterms = term_queue_get_terms($queue->qid);
  foreach ($qterms as $qterm){
    $form[$qterm->tid]['#qterm'] = (array)$qterm;
    $form[$qterm->tid]['name'] = array(
      '#value' => $qterm->name,
    );
    $form[$qterm->tid]['weight'] = array(
      '#type' => 'weight',
      '#delta' => '20',
      '#default_value' => $qterm->weight,
    );
    $form[$qterm->tid]['delete'] = array(
      '#value' => l(t('Delete'), "admin/build/term_queue/{$qterm->qid}/{$qterm->tid}/delete"),
    );
  }

  $form['#empty_text'] = t('No terms have been added.');  
  
  if (count($qterms) > 0) {
    $form['submit'] = array('#type' => 'submit', '#value' => 'Save');
  }  
  return $form;
}

/**
 * Submit handler to save the reordering or queue terms.
 *
 * @see taxonomy_overview_terms()
 */
function term_queue_term_form_submit($form, &$form_state) {
  $queue = $form['#queue'];
  foreach ($form_state['values'] as  $tid => $qterm) {
    if (is_numeric($tid) && $form[$tid]['#qterm']['weight'] != $qterm['weight']) {
      term_queue_update_term($queue['qid'], $tid, $qterm['weight']);
    }
  }
  return;
}


/**
 * Theme the terms overview as a sortable list of terms.
 *
 * @ingroup themeable
 * @see term_queue_term_form()
 */
function theme_term_queue_term_form($form) {
  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['#qterm'])) {
      $element = &$form[$key];
      $row = array();
      $row[] = drupal_render($element['name']);
      $element['weight']['#attributes']['class'] = 'term-queue-weight';
      $row[] = drupal_render($element['weight']);
      $row[] = drupal_render($element['delete']);
      $rows[] = array('data' => $row, 'class' => 'draggable');
    }    
  }
  
  if (empty($rows)) {
    $rows[] = array(array('data' => $form['#empty_text'], 'colspan' => '3'));
  }

  drupal_add_tabledrag('term-queue', 'order', 'sibling', 'term-queue-weight');
  $header = array(t('Name'), t('Weight'), t('Operations'));
  $output = theme('table', $header, $rows, array('id' => 'term-queue'));
  $output .= drupal_render($form);
  return $output;
}

/**
 * Form builder for the term delete form.
 *
 * @ingroup forms
 * @see term_queue_delete_term_form_submit()
 */
function term_queue_delete_term_form(&$form_state, $queue, $tid) {
  $form['qid'] = array('#type' => 'value', '#value' => $queue->qid);
  $form['tid'] = array('#type' => 'value', '#value' => $tid);
  $term = taxonomy_get_term($tid);

  return confirm_form($form,
    t('Are you sure you want to remove %term from the queue %queue?', array('%term' => $term->name, '%queue' => $queue->title)), 
    "admin/build/term_queue/$queue->qid",
    NULL,
    t('Delete'), 
    t('Cancel'));
}

/**
 * Submit handler to delete a term after confirmation, pull the trigger.
 *
 * @see term_queue_delete_term_form()
 */
function term_queue_delete_term_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  term_queue_delete_term($values['qid'], $values['tid']);
  $form_state['redirect'] = 'admin/build/term_queue/' . $values['qid'];
  return;
}



